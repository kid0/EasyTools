# -*- coding: utf-8 -*-
from logic.dictview import dict_view
from ui_base import Ui_Dialog

class DictViewUI(Ui_Dialog):
    
    def __init__(self):
        super().__init__()
        self._vars['dict_view'] = '1.0.0.0'
    
    def dictview_init(self):
        self.dictview_button_run.clicked.connect(self.dictview_button_run_down)
        
    def dictview_button_run_down(self):
        """执行按钮"""
        text = str(self.dictview_textEdit_text.toPlainText())
        care_list = self.dictview_checkBox_list_enter.isChecked()
        last = dict_view(text, care_list)
        self.dictview_textEdit_return.setText(last)