# -*- coding:utf-8 -*-
import re

def replace_all(text='', char='', new_char=''):
    """
    覆盖所有字段
    传入：
        text      str 旧字符串
        char      str 关键字
        new_char  str 新关键字
    """
    return text.replace(char,new_char)

def replace_one_by_one(text='', char='', new_char='', auto_add=True, change_char='@'):
    """
    逐行覆盖
    传入：
        text         str  旧字符串
        char         str  关键字
        new_char     str  新关键字
        auto_add     bool 自动填充自增数
        change_char  str  指代自增数
    """
    lines = text.split('\n')
    new = ''
    i = 0
    for line in lines:
        #第一行是不需要回车的
        if line == lines[0]:
            pass
        else:
            new += '\n'
        #if line == lines[-1] and not line:
            #break            
        if char in line:
            i = i+1
        else:
            new += line
            continue
        if auto_add:
            new += line.replace(char,new_char+str(i))
        else:
            new_char_now = new_char.replace(change_char,str(i))
            new = new + line.replace(char,new_char_now)
    return new

def turn_TF(text='', must_same=False):
    """
    True/False翻转
    传入：
        text       str   旧字符串
        must_same  bool  全词匹配
    """
    if not must_same:
        text = text.replace('True','#@.true.@#')
        text = text.replace('False','#@.false.@#')
        text = text.replace('#@.true.@#','False')
        text = text.replace('#@.false.@#','True')
        return text
    else:
        text = '\n' + text + '\n'
        text = text.replace('\n',' \n ')
        text = text.replace(' True ','#@.true.@#')
        text = text.replace(' False ','#@.false.@#')
        text = text.replace('#@.true.@#',' False ')
        text = text.replace('#@.false.@#',' True ')
        text = text.replace(' \n ','\n')
        return text[1:-1]

def turn_LR(text=''):
    """
    =号左右互换
    传入：
        text    str    字符串
    """
    lines = text.split('\n')
    res = []
    for line in lines:
        if '=' in line:
            a, d, b = re.findall('([\s\S]*?)([ ]*=[ ]*)([\s\S]*)', line)[0]
            res.append(b + d + a)
        else:
            res.append(line)
    return '\n'.join(res)

def deleate_head(text=''):
    """
    去除头空格
    传入：
        text     str     字符串
    """
    lines = text.split('\n')
    return_text = ''        
    for line in lines:
        if line == lines[0]:
            pass
        else:
            return_text += '\n'  
        try:
            key = re.findall('(^[ |]+)',line)[0]
        except:
            key = ''
        return_text = return_text + line.replace(key,'',1)
    return return_text

def deleate_back(text=''):
    """
    去除尾空格
    传入：
        text     str     字符串
    """
    lines = text.split('\n')
    return_text = ''        
    for line in lines:
        if line == lines[0]:
            pass
        else:
            return_text += '\n'
        try:
            key = re.findall('(^[ |]+)',line[::-1])[0]
        except:
            key = ''
        return_text = return_text + line[::-1].replace(key,'',1)[::-1] 
    return return_text

def deleate_strip(text=''):
    """
    去除头尾空格
    传入：
        text     str     字符串
    """
    lines = text.split('\n')
    return_text = ''        
    for line in lines:
        if line == lines[0]:
            pass
        else:
            return_text += '\n'  
        key = key.strip()
    return return_text    

def add_by_head(text='', head=''):
    """
    添加头
    传入：
        text    str    旧字符串
        head    str    需要添加的头
    """
    _text = '\n' + text
    return _text.replace('\n', '\n' + head)[1:]

def add_by_end(text='', end=''):
    """
    添加尾
    传入：
        text    str    旧字符串
        end     str    需要添加的尾
    """    
    if text[-1] == '\n':
        return text.replace('\n', end + '\n')
    else:
        _text = text + '\n'
        return _text.replace('\n', end + '\n')[:-1]


def symbol_alignment(text='', symbols=''):
    """
    符号对齐
    传入：
        text    str   旧字符串
        symbols str   字符关键字
    """
    #[\u4e00-\u9fa5]+ 正则也行
    _symbols = []
    __ = symbols.split(' ')
    for _ in __:
        if not _:
            continue
        else:
            _symbols.append(_)
    if text[-1] == '\n':
        text = text[:-1]
    #切割出来
    lline = []
    #登记每断的应当长度 
    maxlen = {}
    for key in _symbols:
        maxlen[key] = 0
    lines = text.split('\n')
    for line in lines:
        l = line
        split_list = []
        for key in _symbols:
            if key in l:
                s = l.split(key,1)
                split_list.append(s[0])
                l = s[1]
                the_len = (len(s[0].encode())-len(s[0]))//2 + len(s[0])   # encode中文长度会x3，但实际宽度是2
                if the_len > maxlen[key]:
                    maxlen[key] = the_len
            else:
                split_list.append('')
        split_list.append(l)
        lline.append(split_list)
    true_text = ''
    for line in lline:
        for key,s in zip(_symbols,line):
            r = '{:<%d}'%(maxlen[key]-(len(s.encode())-len(s))//2)
            true_text += r.format(s) + key
        true_text += line[-1] + '\n'
    
    return true_text
    for key in _symbols:
        lines = text.split('\n')
        if not lines[-1]:
            lines = lines[:-1]
        return_text = ''
        max_index = 0
        for line in lines:
            if line == lines[-1] and not line:
                break
            #for k in _symbols:
                #_r = '([ ]*{})'.format(k)
                #del_ks = re.findall(_r,line)
                #if del_ks:
                    #del_k = del_ks[0]
                    #line = line.replace(del_k,k)
            index = line.find(key)
            if index > max_index:
                max_index = index
        for line in lines:
            if line == lines[-1] and not line:
                break            
            if key in line:
                msg = line.split(key,1)
                return_text = return_text + msg[0] + ' '*(max_index-len(msg[0])) + key + msg[1] + '\n'
            else:
                return_text = return_text + line + '\n'
        text = return_text
    return text

def end_alignment(text=''):
    """
    结尾空格对齐
    传入：
        text    str    就字符串
    """
    lines = text.split('\n')
    return_text = ''
    max_len = 0
    for line in lines:
        if line == lines[-1] and not line:
            break
        if line[-1] == '\n':
            index = len(line) -1
        else:
            index = len(line)
        if index > max_len:
            max_len = index
    for line in lines:
        if line == lines[-1] and not line:
            break            
        return_text = return_text + line.replace('\n','') + ' '*(max_len-len(line.replace('\n',''))) + '\n'
    return return_text

def replace_by_re(text='', old='', new=''):
    """正则覆盖"""
    lines = text.split('\n')
    new_lines = []
    for line in lines:
        _new_line = re.split(old, line)
        new_line = new.join(_new_line)
        new_lines.append(new_line)
    return '\n'.join(new_lines)

def take_by_re(text='', old=''):
    """正则提取"""
    lines = text.split('\n')
    new_lines = []
    for line in lines:
        new_line = re.findall(old, line)
        if new_line:
            new_line = new_line[0]
        else:
            new_line = ''
        new_lines.append(new_line)
    return '\n'.join(new_lines)