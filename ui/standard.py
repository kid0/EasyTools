# -*- coding:utf-8 -*-
import re
from logic.standard import *
from ui_base import Ui_Dialog


class StandardUI(Ui_Dialog):
    
    def __init__(self):
        super().__init__()
        self._vars['standard'] = '1.1.0.0'
    
    def standard_init(self):
        self.standard_pushButton_use.clicked.connect(self.standard_pushButton_use_down)
        self.standard_pushButton_revoke.clicked.connect(self.standard_pushButton_revoke_down)
        
        self.standard_radioButton_all_replace.clicked.connect(self.standard_radioButton_all_replace_down)
        self.checkBox_enter.clicked.connect(self.checkBox_enter_down)
        self.standard_radioButton_line_replace.clicked.connect(self.standard_radioButton_line_replace_down)
        self.standard_radioButton_TorF.clicked.connect(self.standard_radioButton_TorF_down)
        self.standard_radioButton_LorR.clicked.connect(self.standard_radioButton_LorR_down)
        self.standard_radioButton_ai_replace.clicked.connect(self.standard_radioButton_ai_replace_down)
        self.standard_radioButton_deleate_head.clicked.connect(self.standard_radioButton_deleate_head_down)
        self.standard_radioButton_deleate_back.clicked.connect(self.standard_radioButton_deleate_back_down)
        self.standard_radioButton_add_head.clicked.connect(self.standard_radioButton_add_head_down)
        self.standard_radioButton_add_end.clicked.connect(self.standard_radioButton_add_end_down)
        self.standard_radioButton_beautiful_code.clicked.connect(self.standard_radioButton_beautiful_code_down)
        self.standard_radioButton_beautiful_end.clicked.connect(self.standard_radioButton_beautiful_end_down)
        
        self.standard_radioButton_add_last.clicked.connect(self.standard_radioButton_add_last_down)
        self.standard_radioButton_add_replace.clicked.connect(self.standard_radioButton_add_replace_down)
        
        self.standard_radioButton_ai_add_last.clicked.connect(self.standard_radioButton_ai_add_last_down)
        self.standard_radioButton_ai_add_replace.clicked.connect(self.standard_radioButton_ai_add_replace_down)
        
        self.standard_button_run.clicked.connect(self.replace_run)
        
        self.standard_radioButton_re_replace.clicked.connect(self.standard_radioButton_re_replace_down)
        self.standard_radioButton_re_take.clicked.connect(self.standard_radioButton_re_take_down)
        
        self.standard_radioButton_all_replace_down()
        self.old = ''
        self.new = ''
        
    def replace_run(self):
        if self.standard_radioButton_all_replace.isChecked():
            return self.replace_all()
        if self.standard_radioButton_line_replace.isChecked():
            return self.replace_line()
        if self.standard_radioButton_TorF.isChecked():
            return self.replace_TF()
        if self.standard_radioButton_LorR.isChecked():
            return self.replace_LR()
        if self.standard_radioButton_ai_replace.isChecked():
            return self.replace_ai()
        if self.standard_radioButton_deleate_head.isChecked():
            return self.replace_head()
        if self.standard_radioButton_deleate_back.isChecked():
            return self.replace_back()
        if self.standard_radioButton_add_head.isChecked():
            return self.add_head()
        if self.standard_radioButton_add_end.isChecked():
            return self.add_end()
        if self.standard_radioButton_beautiful_code.isChecked():
            return self.beautiful_code()
        if self.standard_radioButton_re_replace.isChecked():
            return self.replace_re()
        if self.standard_radioButton_re_take.isChecked():
            return self.take_re()
    def replace_all(self):
        #覆盖所有
        old = str(self.standard_textEdit_text.toPlainText())
        old_char = str(self.standard_lineEdit_all_replace_old.text())
        if self.checkBox_enter.isChecked():
            new_char = '\n'
        else:
            new_char = str(self.standard_lineEdit_all_replace_new.text())
        self.standard_textEdit_return.setText(replace_all(old,old_char,new_char))
    def replace_line(self):
        #覆盖一行
        text = str(self.standard_textEdit_text.toPlainText())
        old_char = str(self.standard_lineEdit_line_old.text())
        new_char = str(self.standard_lineEdit_line_new.text())        
        change_char = str(self.standard_lineEdit_line_replace_char.text())
        auto_number = self.standard_radioButton_add_last.isChecked()
        self.standard_textEdit_return.setText(replace_one_by_one(text,old_char,new_char,auto_number,change_char))
    def replace_TF(self):
        #TF翻转
        text = str(self.standard_textEdit_text.toPlainText())
        must_same = self.standard_checkBox_TF_all_same.isChecked()
        self.standard_textEdit_return.setText(turn_TF(text,must_same))
    def replace_LR(self):
        #左右互换
        text = str(self.standard_textEdit_text.toPlainText())
        self.standard_textEdit_return.setPlainText(turn_LR(text))
    def replace_head(self):
        #干掉开头空白
        text = str(self.standard_textEdit_text.toPlainText())
        self.standard_textEdit_return.setPlainText(deleate_head(text))
    def replace_back(self):
        #干掉结尾空白
        text = str(self.standard_textEdit_text.toPlainText())
        self.standard_textEdit_return.setPlainText(deleate_back(text))
    def add_head(self):
        #添加头
        text = str(self.standard_textEdit_text.toPlainText())
        add_key = str(self.standard_lineEdit_add_key.text())
        self.standard_textEdit_return.setPlainText(add_by_head(text,add_key))
    def add_end(self):
        text = str(self.standard_textEdit_text.toPlainText())
        add_key = str(self.standard_lineEdit_add_key.text())
        self.standard_textEdit_return.setPlainText(add_by_end(text,add_key))
    def beautiful_code(self):
        #关键字对齐
        text = str(self.standard_textEdit_text.toPlainText())
        key = str(self.standard_lineEdit_beautiful_code_key.text())
        self.standard_textEdit_return.setPlainText(symbol_alignment(text,key))
    def beautiful_end(self):
        #结尾对齐
        text = str(self.standard_textEdit_text.toPlainText())
        self.standard_textEdit_return.setPlainText(end_alignment(text))        
    def replace_ai(self):
        #智能替换
        text = str(self.standard_textEdit_text.toPlainText())
        old_char = str(self.standard_lineEdit_ai_old.text())
        new_char = str(self.standard_lineEdit_ai_new.text())        
        lines = text.split('\n')
        change_char = str(self.standard_lineEdit_ai_replace_char.text())
        new = ''
        i = 1
        bc = -1
        for line in lines:
            bc_bak = 0
            for j in line:
                if j == ' ':
                    bc_bak = bc_bak + 1
                else:
                    break
            if bc == -1 :
                if old_char in line:
                    bc = bc_bak
                else:
                    pass
            else:
                if bc == bc_bak:
                    i = i + 1
            if self.standard_radioButton_ai_add_last.isChecked():
                new = new + line.replace(old_char,new_char+str(i))+'\n'
            else:
                new_char_bak = new_char.replace(change_char, str(i))
                new = new + line.replace(old_char,new_char_bak)+'\n'
        self.standard_textEdit_return.setText(new)
        
    def replace_re(self):
        """正则覆盖"""
        text = str(self.standard_textEdit_text.toPlainText())
        old_char = str(self.standard_lineEdit_re_old.text())
        new_char = str(self.standard_lineEdit_re_new.text())        
        self.standard_textEdit_return.setPlainText(replace_by_re(text, old_char, new_char))
        
    def take_re(self):
        """正则提取"""
        text = str(self.standard_textEdit_text.toPlainText())
        key = str(self.standard_lineEdit_re_take_old.text())     
        self.standard_textEdit_return.setPlainText(take_by_re(text, key))        
        
    #==========================按钮响应部分=============================
    def checkBox_enter_down(self):
        self.standard_lineEdit_all_replace_new.setEnabled(not self.checkBox_enter.isChecked())
        
    
    def standard_pushButton_use_down(self):
        self.old = self.standard_textEdit_text.toPlainText()
        self.new = self.standard_textEdit_return.toPlainText()
        self.standard_textEdit_text.setPlainText(self.standard_textEdit_return.toPlainText())
        self.standard_textEdit_return.clear()
    #button lo
    def standard_pushButton_revoke_down(self):
        self.standard_textEdit_text.setPlainText(self.old)
        self.standard_textEdit_return.setPlainText(self.new)
    #G 1
    def standard_radioButton_all_replace_down(self):
        self.standard_radioButton_set_group1(1)
    def standard_radioButton_line_replace_down(self):
        self.standard_radioButton_set_group1(2)
    def standard_radioButton_TorF_down(self):
        self.standard_radioButton_set_group1(3)
    def standard_radioButton_LorR_down(self):
        self.standard_radioButton_set_group1(4)
    def standard_radioButton_ai_replace_down(self):
        self.standard_radioButton_set_group1(5)
    def standard_radioButton_deleate_head_down(self):
        self.standard_radioButton_set_group1(6)
    def standard_radioButton_deleate_back_down(self):
        self.standard_radioButton_set_group1(7)
    def standard_radioButton_add_head_down(self):
        self.standard_radioButton_set_group1(8)
    def standard_radioButton_add_end_down(self):
        self.standard_radioButton_set_group1(9)
    def standard_radioButton_beautiful_code_down(self):
        self.standard_radioButton_set_group1(10)
    def standard_radioButton_beautiful_end_down(self):
        self.standard_radioButton_set_group1(11)
    def standard_radioButton_re_replace_down(self):
        self.standard_radioButton_set_group1(12)
    def standard_radioButton_re_take_down(self):
        self.standard_radioButton_set_group1(13)
    def standard_radioButton_set_group1(self,number=1):
        #全部替换
        flag = number == 1
        self.checkBox_enter.setEnabled(flag)
        self.standard_radioButton_all_replace.setChecked(flag)
        self.standard_lineEdit_all_replace_old.setEnabled(flag)
        self.standard_lineEdit_all_replace_new.setEnabled(flag and not self.checkBox_enter.isChecked())
        #逐行替换
        flag = number == 2
        self.standard_radioButton_line_replace.setChecked(flag)
        self.standard_lineEdit_line_old.setEnabled(flag)
        self.standard_lineEdit_line_new.setEnabled(flag)
        self.standard_radioButton_add_last.setEnabled(flag)
        self.standard_radioButton_add_replace.setEnabled(flag)
        self.standard_lineEdit_line_replace_char.setEnabled(flag)
        if flag:
            self.standard_radioButton_add_last_down()
        else:
            self.standard_radioButton_add_last.setChecked(flag)
            self.standard_radioButton_add_replace.setChecked(flag)
        #TF翻转
        flag = number == 3
        self.standard_radioButton_TorF.setChecked(flag)
        self.standard_checkBox_TF_all_same.setEnabled(flag)
        #左右互换
        flag = number == 4
        self.standard_radioButton_LorR.setChecked(flag)
        #智能替换
        flag = number == 5
        self.standard_radioButton_ai_replace.setChecked(flag)
        self.standard_lineEdit_ai_old.setEnabled(flag)
        self.standard_lineEdit_ai_new.setEnabled(flag)
        self.standard_radioButton_ai_add_last.setEnabled(flag)
        self.standard_radioButton_ai_add_replace.setEnabled(flag)
        self.standard_lineEdit_ai_replace_char.setEnabled(flag)
        if flag:
            self.standard_radioButton_ai_add_last_down()
        else:
            self.standard_radioButton_ai_add_last.setChecked(flag)
            self.standard_radioButton_ai_add_replace.setChecked(flag)
        #清除开头空白
        flag = number == 6
        self.standard_radioButton_deleate_head.setChecked(flag)
        #清除结尾空白
        flag = number == 7
        self.standard_radioButton_deleate_back.setChecked(flag)
        
        flag = number in (8,9)
        self.standard_lineEdit_add_key.setEnabled(flag)
        #开头追加
        flag = number == 8
        self.standard_radioButton_add_head.setChecked(flag)
        #结尾追加
        flag = number == 9
        self.standard_radioButton_add_end.setChecked(flag)
        #符号对齐
        flag = number == 10
        self.standard_radioButton_beautiful_code.setChecked(flag)
        self.standard_lineEdit_beautiful_code_key.setEnabled(flag)
        #结尾对齐
        flag = number == 11
        self.standard_radioButton_beautiful_end.setChecked(flag)
        #正则覆盖
        flag = number == 12
        self.standard_radioButton_re_replace.setChecked(flag)
        self.standard_lineEdit_re_old.setEnabled(flag)
        self.standard_lineEdit_re_new.setEnabled(flag)
        #正则提取
        flag = number == 13
        self.standard_radioButton_re_take.setChecked(flag)
        self.standard_lineEdit_re_take_old.setEnabled(flag)
        
    #G 2
    def standard_radioButton_add_last_down(self):
        self.standard_radioButton_set_group2(1)
    def standard_radioButton_add_replace_down(self):
        self.standard_radioButton_set_group2(2)
    def standard_radioButton_set_group2(self,number=1):
        if number==1:
            flag = True
        else:
            flag = False
        self.standard_radioButton_add_last.setChecked(flag)
        if number==2:
            flag = True
        else:
            flag = False
        self.standard_radioButton_add_replace.setChecked(flag)
        self.standard_lineEdit_line_replace_char.setEnabled(flag)
    #G 3
    def standard_radioButton_ai_add_last_down(self):
        self.standard_radioButton_set_group3(1)
    def standard_radioButton_ai_add_replace_down(self):
        self.standard_radioButton_set_group3(2)
    def standard_radioButton_set_group3(self,number=1):
        if number==1:
            flag = True
        else:
            flag = False
        self.standard_radioButton_ai_add_last.setChecked(flag)
        if number==2:
            flag = True
        else:
            flag = False
        self.standard_radioButton_ai_add_replace.setChecked(flag)
        self.standard_lineEdit_ai_replace_char.setEnabled(flag)      