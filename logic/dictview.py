# -*- coding:utf-8 -*-

def dict_view(text='', care_list=True):
    """字典可视化界面"""
    #括号等级
    level = 0
    #最后结果
    last = ''
    #列表换行，每一个非字符串[都+1，]则-1
    is_list = 0
    #这是字符串
    is_str = 0
    #这字符串的标记是
    str_ = ''
    #前一个字符（主要考虑到\"的情况）
    font_str = ''
    #是否刚换行
    after_next_rol = False
    for x in text:
        #刚换行不保留空白
        if after_next_rol and x == ' ':
            continue
        else:
            after_next_rol = False
        #前一个是\都可以忽略[等级1，排除\\的舞蹈]
        if font_str == '\\':
            font_str = x
            last = last + x
            continue
        #[等级2.排除'"的误判]
        if x in ('\'', '\"'):
            #如果没有标志位，那就是表示字符串的开始
            if not str_:
                str_ = x
                font_str = x
                is_str = 1
                last = last + x
                continue
            #与标记一样，那就是结尾
            if x == str_:
                str_ = ''
                font_str = x
                is_str = 0
                last = last + x
                continue
        #[等级3，字符串的处理]
        if is_str:
            last = last + x
            continue
        #if is_list == x:
            #last = last + x
            #continue
        if x == '{':
            level = level + 1
            last = last + x + '\n' + ' '*4*level
            after_next_rol = True
            continue
        if x == '}':
            level = level - 1
            last = last + '\n' + ' '*4*level+ x 
            after_next_rol = True
            continue
        if x == ',':
            if not care_list and is_list:
                last = last + x
            else:
                last = last + x + '\n' + ' '*4*level
                after_next_rol = True
            continue
        if x == '[':
            is_list = is_list + 1
        if x == ']':
            is_list = is_list - 1
        last = last + x
    return last